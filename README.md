# Mathezirkel Skripte

This project contains some scripts for the organization of the Mathezirkel Augsburg.

## Authors and acknowledgment
Most work was done by Kathrin Helmsauer, Sven Prüfer, Anna Rubeck and Felix Stärk.

## License
This project is licenced unter [MIT](https://opensource.org/license/mit), see [LICENSE](./LICENSE).
