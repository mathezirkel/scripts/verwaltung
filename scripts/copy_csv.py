#!/usr/bin/python
# -*- coding: utf-8 -*-

import datetime
from enum import StrEnum
import filecmp
import logging
import os
from pathlib import Path
import shutil
from unicodedata import normalize

EVENT_NAMES = [
    "Präsenzzirkel",
    "Korrespondenzzirkel",
    "Themenzirkel"
]

DATA_DIR_NAMES = ("daten", "Daten")

class Colors(StrEnum):
    HEADER = "\033[95m"
    BOLD = "\033[1m"
    UNCHANGED = "\033[94m"
    CREATED = "\033[92m"
    UPDATED = "\033[93m"
    ENDC = "\033[0m"

logging.basicConfig(level=logging.INFO, format="%(message)s")
logger = logging.getLogger()

def get_event_year() -> int:
    now = datetime.datetime.now()
    return now.year + (1 if now.month >= 9 else 0)

def get_mathezirkel_dir() -> Path:
    try:
        mathezirkel_dir = Path(os.environ["MATHEZIRKEL_DIR"])
    except KeyError as e:
        raise EnvironmentError("Environment variable 'MATHEZIRKEL_DIR' not found.") from e
    if not mathezirkel_dir.exists():
        raise FileNotFoundError(f"MATHEZIRKEL_DIR does not point to a valid directory: {mathezirkel_dir}")
    return mathezirkel_dir

def get_src_event_dir(src_path: Path, event_name: str, event_year: int) -> Path:
    for d in src_path.glob("*/"):
        normalized_dir = normalize("NFC", d.name)
        if event_name in normalized_dir and str(event_year) in normalized_dir:
            return d
    raise FileNotFoundError(f"Event directory for '{event_name}' and year '{event_year}' not found.")

def get_dest_data_dir(zirkel_dir: Path) -> Path:
    for name in DATA_DIR_NAMES:
        dest_data_dir = zirkel_dir / name
        if dest_data_dir.is_dir():
            return dest_data_dir
    raise FileNotFoundError(f"There is no data directory: {zirkel_dir}")

def process_file(src_file: Path, dest_data_dir: Path) -> None:
    if (dest_file := dest_data_dir / src_file.name).exists():
        if filecmp.cmp(src_file, dest_file, shallow=False):
            logger.info("%sUnchanged: %s%s", Colors.UNCHANGED, src_file.name, Colors.ENDC)
            return
        logger.info("%sUpdated: %s%s", Colors.UPDATED, src_file.name, Colors.ENDC)
    else:
        logger.info("%sCreated: %s%s", Colors.CREATED, src_file.name, Colors.ENDC)
    shutil.copy2(src_file, dest_data_dir)

def main() -> None:
    event_year = get_event_year()
    event_school_year = f"{(event_year - 1) % 2000}{event_year % 2000}"

    mathezirkel_dir = get_mathezirkel_dir()
    src_path = mathezirkel_dir / "app-data"
    dest_path = mathezirkel_dir / "mathezirkel" / event_school_year

    for event_name in EVENT_NAMES:
        logger.info("%s%sProcessing Event %s:%s\n", Colors.HEADER, Colors.BOLD, event_name, Colors.ENDC)
        src_event_dir = get_src_event_dir(src_path, event_name, event_year)
        for src_zirkel_path in src_event_dir.glob("*/"):
            src_zirkel_dir = src_zirkel_path.as_posix().rsplit("/", maxsplit=1)[-1]
            if "--" not in src_zirkel_dir:
                continue
            zirkel_name = src_zirkel_dir.rsplit("--", maxsplit=1)[-1]
            logger.info("%sProcessing Zirkel%s: %s", Colors.BOLD, Colors.ENDC, zirkel_name)
            dest_data_dir = get_dest_data_dir(dest_path / event_name / zirkel_name)
            logger.debug("%s -> %s", src_zirkel_dir, dest_data_dir)
            for src_file in src_zirkel_path.glob("*"):
                process_file(src_file, dest_data_dir)
            logger.info("")
        logger.info("")

if __name__ == "__main__":
    main()
