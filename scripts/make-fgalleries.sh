#!/bin/sh

if [ ! -d "./fgalleries" ]; then
    mkdir ./fgalleries
fi

for d in ./src/*; do
    dirname=$(basename "${d}")
    if [ -d "${d}" ]; then
        rm -R "./fgalleries/${dirname}"
    fi
    echo "Processing ${dirname}"
    fgallery "${d}" "./fgalleries/${dirname}"
done
