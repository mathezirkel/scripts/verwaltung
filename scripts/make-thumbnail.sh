#!/bin/sh

DOCUMENT=anmeldeformular_2023

convert -thumbnail x500 -flatten "${DOCUMENT}.pdf[0]" "${DOCUMENT}"_vorschau.jpg
